#!/usr/bin/env python3

from distutils.core import setup
import uptomate


setup(name='UpToMate',
      version=uptomate.__version__,
      description='Small wrapper lib for using python-vagrant to organize VMs',
      author='rugo',
      url="https://gitlab.com/rugo/uptomate",
      author_email='rg@ht11.org',
      packages=['uptomate'],
      install_requires=['python-vagrant'],
)