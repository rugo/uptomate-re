# UpToMate

This is a really small lib, written to manage VMs with vagrant.

It uses the `python-vagrant` library.

For examples how to use, see `test.py` or the project `berlyne` which this was developed for.