import uptomate
import unittest
import os
import time
from urllib.request import urlopen


class UpToMateTest(unittest.TestCase):

    def setUp(self):
        self.depl = uptomate.Deployment.Vagrant(
            "apache-testcase",
            deployment_path="testfiles"
        )
        self.depl.install(os.getcwd() + "/vagrantfiles/ubuntu_uni_test")

    def tearDown(self):
        self.depl.destroy()
        self.assertFalse(
           self.depl.installed
        )
        # Needs some time or vagrant will think the vm is still up
        time.sleep(2)

    # Its not nice to have a test case that tests so many methods
    # but my life is to short to always wait until the box was set up again
    def run_box(self, provider: uptomate.Provider.GenericProvider, port=8080, state="running"):
        self.assertTrue(self.depl.installed)

        # check start, status and hostname
        self.depl.start(provider=provider)
        self.assertEqual(self.depl.status().state, state)

        self.assertIsNotNone(self.depl.hostname())

        self.assertEqual(
            urlopen(
                "http://{}:{}".format(self.depl.service_network_address(), port)
            ).readall().strip().decode(),
            "test"
        )

    def test_docker(self):
        self.run_box(uptomate.Provider.docker_provider)

    def test_symlink(self):
        # Create a symlink
        link_path = "/tmp/test_utm"
        self.depl.create_content_symlink("var/www/html/index.html", link_path)
        with open(link_path) as f:
            self.assertEqual(f.readline().strip(), 'test')
        os.unlink(link_path)

    def test_virtualbox(self):
        self.run_box(uptomate.Provider.virtual_box_provider)

    def test_digitalocean(self):
        self.run_box(uptomate.Provider.digital_ocean_provider, 80, "active")

if __name__ == '__main__':
    uptomate.Deployment.TEST_MODE = True
    unittest.main(verbosity=2)
