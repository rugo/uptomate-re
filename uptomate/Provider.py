LOCALHOST = "127.0.0.1"


class GenericProvider:
    name = ""
    port_forwarding = True

    @staticmethod
    def get_accessible_address(hostname):
        return LOCALHOST

    def __str__(self):
        # easier, since there is no need to differentiate between instances
        return self.__class__.__name__


class DockerProvider(GenericProvider):
    pass


class VirtualBoxProvider(GenericProvider):
    pass


class DigitalOceanProvider(GenericProvider):
    port_forwarding = False

    @staticmethod
    def get_accessible_address(hostname):
        return hostname


docker_provider = DockerProvider()
digital_ocean_provider = DigitalOceanProvider()
virtual_box_provider = VirtualBoxProvider()


ALLOWED_PROVIDERS = {
    "docker": docker_provider,
    "digital_ocean": digital_ocean_provider,
    "virtualbox": virtual_box_provider
}
