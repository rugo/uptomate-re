import os
import shutil
import vagrant
import json
from zipfile import ZipFile
from subprocess import CalledProcessError
from .Provider import ALLOWED_PROVIDERS


CONFIG_FILE_NAME = "config.json"
TASK_CONTENT_FILE_NAME = "task.zip"
DEFAULT_FILE_NAMES = [CONFIG_FILE_NAME, "setup", TASK_CONTENT_FILE_NAME]
VAGRANTFILE_NAME = "Vagrantfile"
INSTALLED_MARKER_FILE = "installed"
PORTS_CONFIG_FILE = "ports.json"
PROBLEM_FOLDER_NAME = "problem"

INSTANCE_DIR_NAME = "instance"
CONTENT_DIR_NAME = "content"

_DEFAULT_DIR_MODE = 0o770

# Unfort. some vagrant plugins use different names for
# the same thing. With this table, the status names are unified.
_STATUS_TRANSLATION = {
    # digital ocean plugin has status "active" for "running"
    'active': 'running',
    # VirtualBox says "poweroff" instead of "stopped"
    'poweroff': 'stopped'
}

VAGRANT_RUNNING = 'running'
VAGRANT_STOPPED = 'stopped'
VAGRANT_NOT_CREATED = "not_created"
VAGRANT_UNKNOWN = "**unknown**"
VAGRANT_RUNNING_STATES = (VAGRANT_RUNNING,)
VAGRANT_STOPPED_STATES = (VAGRANT_STOPPED, VAGRANT_NOT_CREATED)
VAGRANT_STATES = VAGRANT_RUNNING_STATES + VAGRANT_STOPPED_STATES

# Which action causes which state
ASSOCIATED_STATES = {
    'start': VAGRANT_RUNNING,
    'stop': VAGRANT_STOPPED,
    'install': VAGRANT_NOT_CREATED,
    'reload': VAGRANT_RUNNING,
}

# Codes vagrant uses when some or all of the destroy commands
# where declined:
# github.com/mitchellh/vagrant/blob/master/plugins/commands/destroy/command.rb
_DECL_RETCODES = [1, 2]

_joinp = os.path.join
_p_exists = os.path.exists


def _fname(path):
    return os.path.split(path)[-1]


def _move_files(files_dict, dst_folder):
    for f in files_dict:
        f_name = _joinp(dst_folder, _fname(f))

        shutil.move(f, f_name)


def _make_absolute(path):
    if path.startswith("/"):
        return path
    return _joinp(os.getcwd(), path)


def check_installed(method):
    def creator(self, *args, **kwargs):
        if self.installed:
            return method(self, *args, **kwargs)
        else:
            raise ValueError(
                "Deployment '{}' is not installed yet!".format(self)
            )
    return creator


class Vagrant:

    def __init__(self,
                 name,
                 deployment_path="deployments"):
        self.__name = name
        self.__base_path = _joinp(os.path.abspath(deployment_path), name)
        self.__instance_path = _joinp(self.__base_path, INSTANCE_DIR_NAME)
        self.__content_path = _joinp(self.__instance_path, CONTENT_DIR_NAME)
        self.__installed_marker = _joinp(self.__instance_path, INSTALLED_MARKER_FILE)
        self.__vagr = vagrant.Vagrant(self.__instance_path)

    def install(self, vagrant_file_path):
        if not _p_exists(self.__base_path):
            raise ValueError(
                "No Deployment with name '{}' exists!".format(self.__name)
            )
        if _p_exists(self.__instance_path):
            raise ValueError(
                "Deployment '{}' already created. Destroy first.".format(
                    self.__instance_path
                )
            )
        self._create_vagrant_folders()
        src_dir = _joinp(self.__base_path, CONTENT_DIR_NAME)
        os.mkdir(self.__content_path, _DEFAULT_DIR_MODE)
        self._create_vagrant_symlink(vagrant_file_path)
        self._init_ports()
        self.mark_installed()

    def _init_ports(self):
        ports = self.get_config()['ports']
        self.set_ports(ports)

    def set_ports(self, ports):
        with open(_joinp(self.__instance_path, PORTS_CONFIG_FILE), "w") as f:
            json.dump(ports, f, indent=4)

    def _create_vagrant_symlink(self, vagrant_path):
        if not os.path.exists(vagrant_path):
            raise ValueError(
                "Vagrant Deploy environment '{}' does not exist.".format(
                    vagrant_path
                )
            )
        os.symlink(vagrant_path, _joinp(self.__instance_path, VAGRANTFILE_NAME))

    def _create_vagrant_folders(self):
        os.mkdir(self.__instance_path, _DEFAULT_DIR_MODE)

    @property
    def exists(self):
        return os.path.exists(self.__base_path)

    def mark_installed(self):
        open(self.__installed_marker, "w").close()

    def mark_uninstalled(self):
        os.unlink(self.__installed_marker)

    @property
    def installed(self):
        return _p_exists(self.__installed_marker)

    @check_installed
    def start(self, provider=None):
        self.__vagr.up(provider=provider)

    def get_config(self):
        try:
            with open(_joinp(self.__base_path, CONFIG_FILE_NAME)) as f:
                c = json.load(f)
        except OSError:
            raise ValueError("No configfile exists in {}".format(self.path))
        except (TypeError, ValueError) as ex:
            raise ValueError("Config file invalid: {}".format(str(ex)))

        return c

    def normalize_dl_path(self, rel_path, absolut=False):
        """
        Normalizes the given relative path and checks if
        rel_path is in content dir, to avoid path traversal.
        :param rel_path: relative path in content dir
        :param absolut: if True, return absolut path to re_path
        :return: normalized path to rel_path
        """
        dl_folder = _joinp(self.__base_path, CONTENT_DIR_NAME)
        full_path = _joinp(
            dl_folder,
            rel_path
        )
        norm_path = os.path.normpath(
            full_path
        )
        if not norm_path.startswith(dl_folder):
            raise ValueError(
                "Path '{}' is not inside '{}'s content dir".format(
                    dl_folder, self.__name
                )
            )

        if absolut:
            return norm_path

        return norm_path[len(dl_folder)+1:]

    def open_content_file(self, file_name, mode="w"):
        """
        Opens a file in the deployments content dir.
        :return: File handle
        """
        return open(_joinp(self.__content_path, file_name), mode)

    @check_installed
    def stop(self):
        self.__vagr.halt()

    @check_installed
    def resume(self):
        self.__vagr.resume()

    @check_installed
    def reload(self, provision=True):
        self.__vagr.reload(provision=provision)

    @check_installed
    def suspend(self):
        self.__vagr.suspend()

    @check_installed
    def status(self):
        status = self.__vagr.status()[0]
        if status.state in _STATUS_TRANSLATION:
            status = vagrant.Status(
                name=status.name,
                state=_STATUS_TRANSLATION[status.state],
                provider=status.provider
            )
        return status

    @check_installed
    def find_provider(self):
        """
        Tries to get the provider from vagrant.
        If that fails, None is returned
        :return: Provider or None
        """
        try:
            status = self.status()
            return ALLOWED_PROVIDERS[status.provider]
        except CalledProcessError:
            return None

    @check_installed
    def hostname(self):
        return self.__vagr.hostname()

    @check_installed
    def service_network_address(self):
        """
        Tries to get the services address.
        If no provider can be found, None is returned
        :return: Address as string or None
        """
        provider = self.find_provider()
        if not provider:
            return None
        return provider.get_accessible_address(self.hostname())

    @check_installed
    def destroy(self):
        try:
            self.__vagr.destroy()
        except CalledProcessError as ex:
            if ex.returncode not in _DECL_RETCODES:  # something weird happened
                raise ex
        shutil.rmtree(self.__instance_path)

    def __str__(self):
        return "Vagrant: '{}'".format(self.__name)